SPRINGBOOT DEMO系统
====== 
 
 
 该项目使用了spring-boot、spring-data-jpa、gradle、lombok等技术，实现了http的请求服务，并规范了http项目的目录结构。<br>
 该项目的目标是提供常用功能的demo代码，要求简洁、优雅，如果有多种实现方式，选取相对比较好的方式。<br><br>
 该项目提供常用功能的DEMO示例:
  + 多数据源配置 
  + 熔断
  + 全局异常处理
  + 过滤器
  + 分布式锁 
  + 请求log
  + 常用工具类
  + 多表查询、分页查询
  + restful api
  + 阿里云消息服务
  + 单例MYSQL跨DB联表查询
  + 拦截器
  + 集成apidoc、swagger2
  + 集成jmockdata，用于testcase
  + QLExpressTests
  + IKAnalyzer、SimHash算法
  + mockito
  + sharding-sphere
  + gitlab-ci
  + mapStruct
  + redis cache, 多redis
  + pdf功能
  + 阿里云log推送
  + 集成servicedoc（https://www.npmjs.com/package/servicedoc）
  + 集成esper
  + vault
  + lts，任务调度
  + spring-data-redis
  + spring-data-mongo
  + 阿里云oss
  + transaction使用configuration配置
  + testNG
  + poi-tl word模板引擎
  + hibernate entity使用@SqlDelete、@Where应用到软删除上
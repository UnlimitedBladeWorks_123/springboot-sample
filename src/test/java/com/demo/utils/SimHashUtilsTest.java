package com.demo.utils;

import com.demo.BaseTest;
import com.demo.uitls.IKAnalyzerUtils;
import com.demo.uitls.SimHash;
import com.demo.uitls.SimHashUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author DongXL
 * @Create 2018-05-31 18:33
 */
public class SimHashUtilsTest extends BaseTest {

    @Test
    public void testSimHash() {
        try {
            String news = "美国队长（Captain America）是美国漫威漫画旗下超级英雄，初次登场于《美国队长》（Captain America Comics）第1期（1941年3月），由乔·西蒙和杰克·科比联合创造，被视为美国精神的象征。本名史蒂文·“史蒂夫”·罗杰斯（Steven “Steve” Rogers），1920年7月4日出生于美国纽约布鲁克林区，原本是一名身体瘦弱的新兵，在接受美国政府的实验改造后变成了“超级士兵”，这使其力量、速度、耐力等各项体能都远超出于常人，且还被美国政府赋予了由世界上最坚硬的金属之一振金制成的盾牌，从此史蒂夫以美国队长的身份，为美国及世界在二战中立下显赫战功，后来在二战尾声的一次行动中，美国队长与宿敌红骷髅战斗，并掉入大海之中被冰封近70年，直到被神盾局发现并解冻后才加入了复仇者联盟，此后在美国队长的领导下，复仇者联盟出生入死，赢得一次又一次近乎不可能的胜利。";

            List<IKAnalyzerUtils.Word> wordList = IKAnalyzerUtils.INSTANCE.getSplitWordList(news);
            List<String> alist = new ArrayList<>();
            for (IKAnalyzerUtils.Word word : wordList) {
                alist.add(word.getTerm());
            }
            String tokens = StringUtils.join(alist, " ");
            SimHash simHash = SimHashUtils.INSTANCE.getSimHash(tokens);

            String news2 = "《美国队长：复仇者先锋》是由美国漫威影业制作的124分钟科幻动作影片。该片由乔·庄斯顿执导，克里斯·埃文斯、海莉·阿特维尔、塞巴斯蒂安·斯坦、雨果·维文等主演，于2011年7月22日在美国上映、2011年9月9日在中国大陆上映。片改编自同名漫画作品，以二战时期为背景，且还被美国政府赋予了由世界上最坚硬的金属之一振金制成的盾牌，从此史蒂夫以美国队长的身份，讲述了一个叫做史蒂夫·罗杰斯的年轻人参加美军秘密试验，在被注射了超级士兵的血清后成为“美国队长”，与纳粹展开对抗的故事。";
            List<IKAnalyzerUtils.Word> wordList2 = IKAnalyzerUtils.INSTANCE.getSplitWordList(news2);
            List<String> alist2 = new ArrayList<>();
            for (IKAnalyzerUtils.Word word : wordList2) {
                alist2.add(word.getTerm());
            }
            String tokens2 = StringUtils.join(alist2, " ");
            SimHash simHash2 = SimHashUtils.INSTANCE.getSimHash(tokens2);

            String news3 = "蜘蛛侠（Spider-Man）是美国漫威漫画旗下超级英雄，由编剧斯坦·李和画家史蒂夫·迪特科联合创造，初次登场于《惊奇幻想》（Amazing Fantasy）第15期（1962年8月），因为广受欢迎，几个月后，便开始拥有以自己为主角的单行本漫画。他本名彼得·本杰明·帕克（Peter Benjamin Parker），是住在美国纽约皇后区的一名普通高中生，由于被一只受过放射性感染的蜘蛛咬伤，因此获得了蜘蛛一样的超能力，后自制了蛛网发射器，化身蜘蛛侠（Spider-Man）守卫城市。 ";
            List<IKAnalyzerUtils.Word> wordList3 = IKAnalyzerUtils.INSTANCE.getSplitWordList(news3);
            List<String> alist3 = new ArrayList<>();
            for (IKAnalyzerUtils.Word word : wordList3) {
                alist3.add(word.getTerm());
            }

            String tokens3 = StringUtils.join(alist3, " ");
            SimHash simHash3 = SimHashUtils.INSTANCE.getSimHash(tokens3);

            System.out.println(SimHashUtils.INSTANCE.calHammingDistance(simHash, simHash2));
            System.out.println(SimHashUtils.INSTANCE.calHammingDistance(simHash3, simHash2));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

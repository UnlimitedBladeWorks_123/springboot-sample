package com.demo;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TransferQueue;


/**
 * @Author DongXL
 * @Create 2018-05-09 17:13
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class BaseTest {
    public static final int MAXIMUM_CAPACITY = 2147483647;

    @Test
    @Ignore
    public void test() throws Exception {
        TransferQueue<Integer> queue = new LinkedTransferQueue<>();
        new Thread(() -> {
            try {
                for (; ; ) {
                    Thread.sleep(5 * 1000);
                    System.out.println(String.format("%s %s %s", "消费者", new Date(), queue.poll(5, TimeUnit.SECONDS)));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();
        for (int i = 1; i < 10; i++) {
            queue.put(i);
            System.out.println(String.format("%s %s %s", "生产者", new Date(), i));
        }

        Thread.sleep(2000 * 1000);
    }

    final int tableSizeFor(int c) {
        int n = c - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

    @Test
    public void dong() {
        List<Integer> alist = new ArrayList<>();
        Integer a = 1;
        Integer b = 2;
        Integer c = new Integer(1);
        alist.add(a);
        alist.add(b);
        alist.add(a);
        alist.add(a);
        System.out.println(alist);
        alist.remove(c);
        System.out.println(alist);
        Object[] mm = new Object[11];
        mm[0] = a;
        mm[1] = "sss";
        System.out.println(mm);

        System.out.println(tableSizeFor(23));
        System.out.println(tableSizeFor(223));
        System.out.println(tableSizeFor(133));
        System.out.println(tableSizeFor(41));

    }

}

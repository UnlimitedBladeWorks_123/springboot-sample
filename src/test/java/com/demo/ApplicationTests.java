package com.demo;

import com.alibaba.druid.filter.config.ConfigTools;
import com.alibaba.fastjson.JSON;
import com.demo.component.holder.HolderFactory;
import com.demo.model.entity.baoxian.SpringbootDemoEntity;
import com.demo.repository.baoxian.SpringbootDemoRepository;
import com.demo.scheduler.LTSTask;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.jpa.JpaTransactionManager;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Stream;

public class ApplicationTests extends BaseTest {

    @Resource
    private HolderFactory holderFactory;
    @Value("${demo.mns.queue.springbootDemoTest}")
    private String springbootDemoQueueName;
    @Resource
    private SpringbootDemoRepository springbootDemoRepository;
    @Resource
    private JpaTransactionManager transactionManagerBaoxian;
    @Resource
    private JpaTransactionManager transactionManagerTob;
    @Resource
    private LTSTask ltsTask;

    @Test
    public void contextLoads() throws Exception {
        String str = null;
        StringBuffer sb = new StringBuffer();
        sb.append("12321313=");
        sb.append(str);
        System.out.println(sb.toString());
        System.out.println(ConfigTools.decrypt("nXaU2BV9ct0pbwwdtpjFYsDT4Y8C7PAr4/Ou7CL+Pkqvu5BEDZPEVha7AypGZSdy+bDTc9nn+pVPiiEu8LVcoA=="));
        System.out.println(ConfigTools.encrypt("v2A7Qz8NBgSKpt4xmoTF9GwTYzgoQC"));
        System.out.println(1);
    }

    @Test
    public void testSendMnsMessage() {
        holderFactory.getMNSClientHelper().sendMsg(springbootDemoQueueName, "mns queue message test!", 60);
    }

    @Test
    public void testStream() {
        List<SpringbootDemoEntity> demoList = springbootDemoRepository.getAllByDeletedFalse();
        demoList.parallelStream().forEach(o -> System.out.println("Item : " + o.getId() + " Count : " + o.getCname()));
        Stream<SpringbootDemoEntity> stream = springbootDemoRepository.streamAllBy();

        System.out.println(JSON.toJSONString(stream.findFirst()));
    }

}

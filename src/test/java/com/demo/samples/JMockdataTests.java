package com.demo.samples;

import com.alibaba.fastjson.JSON;
import com.demo.BaseTest;
import com.demo.model.domain.SpringbootDemoMoreDO;
import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.MockConfig;
import org.junit.Test;

/**
 * @Author DongXL
 * @Create 2018-05-09 17:13
 */
public class JMockdataTests extends BaseTest {

    @Test
    public void testJMock() {
        SpringbootDemoMoreDO more = new SpringbootDemoMoreDO();
        MockConfig config = new MockConfig().longRange(111l, 222l);
        more.setId(JMockData.mock(Long.class, config));
        more.setCname(JMockData.mock(String.class));
        System.out.println(JSON.toJSONString(more));
        SpringbootDemoMoreDO more2 = JMockData.mock(SpringbootDemoMoreDO.class);
        System.out.println(JSON.toJSONString(more2));
    }


}

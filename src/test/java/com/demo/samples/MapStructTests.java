package com.demo.samples;

import com.alibaba.druid.filter.config.ConfigTools;
import com.demo.BaseTest;
import org.junit.Test;

/**
 * @Author DongXL
 * @Create 2018-06-29 16:48
 */
public class MapStructTests extends BaseTest {

    @Test
    public void test() throws Exception {
        System.out.println(ConfigTools.decrypt("nKYndUfFAqtl2h01/mnWqHbcFw64qhgOR3+UyH5/FDmXty8yGsZh3gNu9dEknoBS9NJg91C3ya5hp1+HhfBOhQ=="));
        for (int i = 0; i < 200; i++) {
            System.out.println(String.format("____________ i:%d", i));
            System.out.println(String.format("-------- i:%d", i % 25));
            System.out.println(String.format("-------- i:%d", i & 0x19));
            System.out.println("************************************************");
        }
    }


}

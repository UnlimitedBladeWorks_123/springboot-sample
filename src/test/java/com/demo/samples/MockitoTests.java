package com.demo.samples;

import com.demo.BaseTest;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

/**
 * @Author DongXL
 * @Create 2018-06-07 10:00
 */
@Ignore
public class MockitoTests extends BaseTest {

    /**
     * 验证行为
     */
    @Test
    public void testVerify() {
        //一旦创建 mock 将会记得所有的交互
        List mockedList = mock(List.class);
        //using mock object
        mockedList.add("one");
        mockedList.add("two");
        mockedList.clear();

        //验证行为
        verify(mockedList).add("one");
        verify(mockedList).clear();
    }


    /**
     * stubbing
     * 默认情况下，所有方法都会返回值，一个 mock 将返回要么 null，一个原始/基本类型的包装值或适当的空集。例如，对于一个 int/Integer 就是 0，而对于 boolean/Boolean 就是 false。
     * Stubbing 可以被覆盖。
     * 一旦 stub，该方法将始终返回一个 stub 的值，无论它有多少次被调用。
     * 最后的 stubbing 是很重要的 – 当你使用相同的参数 stub 多次同样的方法。换句话说：stubbing 的顺序是重要的，但它唯一有意义的却很少，例如当 stubbing 完全相同的方法调用，或者有时当参数匹配器的使用，等等。
     */
    @Test
    public void testStubbing() {
        //You can mock concrete classes, not just interfaces
        LinkedList mockedList = mock(LinkedList.class);

        //stubbing
        when(mockedList.get(0)).thenReturn("first");
        when(mockedList.get(1)).thenThrow(new RuntimeException());

        //following prints "first"
        System.out.println(mockedList.get(0));

        //following throws runtime exception
        //System.out.println(mockedList.get(1));

        //following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));

        //Although it is possible to verify a stubbed invocation, usually it's just redundant
        //If your code cares what get(0) returns, then something else breaks (often even before verify() gets executed).
        //If your code doesn't care what get(0) returns, then it should not be stubbed. Not convinced? See here.
        verify(mockedList).get(0);
    }


    /**
     * 参数匹配器
     */
    @Test
    public void testArgumentMatch() {

        Map map = mock(Map.class);
        //stubbing using built-in anyInt() argument matcher
        when(map.get(anyInt())).thenReturn("element");
        when(map.get(anyString())).thenReturn("1231232132");
        //stubbing using custom matcher (let's say isValid() returns your own matcher implementation):
        //  when(mockedList.contains(argThat(isValid()))).thenReturn("element");
        //following prints "element"
        System.out.println(map.get(999));
        System.out.println(map.get("你妹"));

        //you can also verify using an argument matcher
        verify(map).get(anyInt());
    }

    /**
     * 调用额外的调用数字/at least x / never
     */
    @Test
    public void testVerifyExtraTimes() {
        LinkedList mockedList = mock(LinkedList.class);

        //using mock
        mockedList.add("once");

        mockedList.add("twice");
        mockedList.add("twice");

        mockedList.add("three times");
        mockedList.add("three times");
        mockedList.add("three times");

        //following two verifications work exactly the same - times(1) is used by default
        verify(mockedList).add("once");
        verify(mockedList, times(1)).add("once");

        //exact number of invocations verification
        verify(mockedList, times(2)).add("twice");
        verify(mockedList, times(3)).add("three times");

        //verification using never(). never() is an alias to times(0)
        verify(mockedList, never()).add("never happened");

        //verification using atLeast()/atMost()
        verify(mockedList, atLeastOnce()).add("three times");
        verify(mockedList, atLeast(2)).add("five times");
        verify(mockedList, atMost(5)).add("three times");
    }

    /**
     * Stubbing void 方法处理异常
     */
    @Test
    public void testDoThrow() {
        LinkedList mockedList = mock(LinkedList.class);
        doThrow(new RuntimeException()).when(mockedList).clear();

        //following throws RuntimeException:
        mockedList.clear();
    }


    /**
     * 有序的验证
     */
    @Test
    public void testVerifyInOrder() {
        // A. Single mock whose methods must be invoked in a particular order
        List singleMock = mock(List.class);

        //using a single mock
        singleMock.add("was added first");
        singleMock.add("was added second");

        //create an inOrder verifier for a single mock
        InOrder inOrder = inOrder(singleMock);

        //following will make sure that add is first called with "was added first, then with "was added second"
        inOrder.verify(singleMock).add("was added first");
        inOrder.verify(singleMock).add("was added second");

        // B. Multiple mocks that must be used in a particular order
        List firstMock = mock(List.class);
        List secondMock = mock(List.class);

        //using mocks
        firstMock.add("was called first");
        secondMock.add("was called second");

        //create inOrder object passing any mocks that need to be verified in order
        InOrder inOrder2 = inOrder(firstMock, secondMock);

        //following will make sure that firstMock was called before secondMock
        inOrder2.verify(firstMock).add("was called first");
        inOrder2.verify(secondMock).add("was called second");

        // Oh, and A + B can be mixed together at will
    }


}

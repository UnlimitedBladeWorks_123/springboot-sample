package com.demo.samples;

import com.demo.BaseTest;
import com.demo.model.document.SpringBootDocument;
import com.demo.repository.mongo.SpringBootDocumentRepository;
import org.junit.Test;

import javax.annotation.Resource;

public class SpringBootRepositoryTests extends BaseTest {

    @Resource
    private SpringBootDocumentRepository springBootDocumentRepository;

    @Test
    public void saveSpringbootDocument(){
        SpringBootDocument document = new SpringBootDocument();
        document.setName("spring boot demo");
        springBootDocumentRepository.save(document);
    }

}

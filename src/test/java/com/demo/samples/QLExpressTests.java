package com.demo.samples;

import com.demo.BaseTest;
import com.demo.model.dto.SpringbootDemoDTO;
import com.ql.util.express.DefaultContext;
import com.ql.util.express.ExpressRunner;
import com.ql.util.express.Operator;
import com.ql.util.express.OperatorOfNumber;
import org.junit.Test;

/**
 * @Author DongXL
 * @Create 2018-05-09 17:18
 */
public class QLExpressTests extends BaseTest {

    class GroupOperator extends Operator {
        public GroupOperator(String aName) {
            this.name = aName;
        }

        public Object executeInner(Object[] list) throws Exception {
            Object result = new Integer(0);
            for (int i = 0; i < list.length; i++) {
                result = OperatorOfNumber.add(result, list[i], true);
            }
            return result;
        }
    }

    @Test
    public void testQLExpress() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        context.put("samples", 1);
        context.put("b", 2);
        context.put("c", 3);
        String express = "samples+b*c";
        Object r = runner.execute(express, context, null, true, false);
        System.out.println(r);
    }

    @Test
    public void testQLExpress222() throws Exception {
        SpringbootDemoDTO dto = new SpringbootDemoDTO();
        dto.setId(13331L);
        SpringbootDemoDTO dto1 = new SpringbootDemoDTO();
        dto1.setId(dto.getId());
        dto1.setId(null);
        System.out.println(111);
    }


    @Test
    public void testQLExpressAlias() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        runner.addOperatorWithAlias("如果", "if", null);
        runner.addOperatorWithAlias("则", "then", null);
        runner.addOperatorWithAlias("否则", "else", null);

        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        context.put("samples", 1);
        String exp = "如果 (如果 1==2 则 false 否则 true) 则 {2+2;} 否则 {20 + 20;}";
        exp = "如果 (samples==1) 则{ 222+111; }否则 40";
        Object r = runner.execute(exp, context, null, false, false, null);
        System.out.println(r);
    }

    @Test
    public void testQLExpressGroupOperator() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        runner.addFunction("累加", new GroupOperator("groups"));

        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        String exp = "累加(2,3,4) ";

        Object r = runner.execute(exp, context, null, false, false, null);
        System.out.println(r);
    }

}

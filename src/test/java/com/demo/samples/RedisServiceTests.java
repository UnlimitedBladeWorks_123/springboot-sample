package com.demo.samples;

import com.demo.BaseTest;
import com.demo.service.RedisService;
import org.junit.Test;

import javax.annotation.Resource;

public class RedisServiceTests extends BaseTest {

    @Resource
    private RedisService redisService;

    @Test
    public void testRedisTemplateDB1setNx() {
        String key = "dong";
        String value = "252";
        int expireSeconds = 100;
        Object status = redisService.redisTemplateDB1setNx(key, value, expireSeconds);
        System.out.println(status);
    }

}

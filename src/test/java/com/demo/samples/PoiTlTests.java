package com.demo.samples;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.*;
import com.deepoove.poi.data.style.TableStyle;
import com.demo.BaseTest;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class PoiTlTests{


    @Test
    public void testDocx() throws IOException {
        Map<String, Object> model = new HashMap<>();
        model.put("title", "Poi-tl 模板引擎");
        RowRenderData header = RowRenderData.build(new TextRenderData("FFFFFF", "姓名"), new TextRenderData("FFFFFF", "学历"));
        TableStyle rowStyle = new TableStyle();
        rowStyle.setBackgroundColor("1FC6E2");
        header.setRowStyle(rowStyle);
        RowRenderData row0 = RowRenderData.build("张三", "研究生");
        RowRenderData row1 = RowRenderData.build("李四", "博士");
        RowRenderData row2 = RowRenderData.build("王五", "博士后");
        model.put("table", new MiniTableRenderData(header, Arrays.asList(row0, row1, row2)));
        model.put("feature", new NumbericRenderData(new ArrayList<TextRenderData>() {
            {
                add(new TextRenderData("Plug-in grammar"));
                add(new TextRenderData("Supports word text, header..."));
                add(new TextRenderData("Not just templates, but also style templates"));
            }
        }));
        List<SegmentData> segments = new ArrayList<>();
        SegmentData s1 = new SegmentData();
        s1.setTitle("经常抱怨的自己");
        s1.setContent("每个人生活得都不容易，经常向别人抱怨的人，说白了就是把对方当做“垃圾场”，你一股脑地将自己的埋怨与不满倒给别人，自己倒是爽了，你有考虑过对方的感受吗？对方的脸上可能一笑了之，但是心里可能有一万只草泥马奔腾而过。");
        segments.add(s1);
        SegmentData s2 = new SegmentData();
        s2.setTitle("拖拖拉拉的自己");
        s2.setContent("能够今天做完的事情，不要拖到明天，你的事情没有任何人有义务去帮你做；不要做“宅男”、不要当“宅女”，放假的日子约上三五好友出去转转；经常动手做家务，既能分担伴侣的负担，又有一个干净舒适的环境何乐而不为呢？");
        segments.add(s2);
        model.put("docx_word", new DocxRenderData(new File("src/test/resources/templates/segment.docx"), segments));

        XWPFTemplate template = XWPFTemplate.compile("src/test/resources/templates/template.docx").render(model);
        FileOutputStream out = new FileOutputStream("src/test/resources/templates/out_template.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }

}

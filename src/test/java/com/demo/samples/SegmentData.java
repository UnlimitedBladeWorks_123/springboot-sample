package com.demo.samples;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class SegmentData implements Serializable {

    private String title;
    private String content;

}

package com.demo;

import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class WinTestNGTests extends AbstractTestNGSpringContextTests {

    private String sql;

    @Parameters({"sql"})
    @BeforeClass
    public void beforeClass(String sql) {
        this.sql = sql;
    }

    @DataProvider(name = "testData")
    public Object[] getData() throws SQLException, ClassNotFoundException {
        return new String[]{"1","2"};
    }

    @Test(dataProvider = "testData")
    public void execute(Object o) {
        System.out.println("TestNG-test1"+ sql);
     }
}

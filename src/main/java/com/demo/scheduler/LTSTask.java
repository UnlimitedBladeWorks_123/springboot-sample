package com.demo.scheduler;

import com.github.ltsopensource.spring.tasktracker.JobRunnerItem;
import com.github.ltsopensource.spring.tasktracker.LTS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@LTS
public class LTSTask {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @JobRunnerItem(shardValue = "springbootSampleJob")
    public void springbootSampleJob() {
        logger.info("LTSTask springbootSampleJob execute!");
    }


}

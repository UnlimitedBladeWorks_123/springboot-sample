package com.demo.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;

@Service
public class RedisService {

    @Resource
    private RedisTemplate redisTemplateDB1;

    public boolean redisTemplateDB1setNx(String key, String value, int expireSeconds) {
        String status = (String) redisTemplateDB1.execute((RedisCallback<String>) connection -> {
            Jedis jedis = (Jedis) connection.getNativeConnection();
            return jedis.set(key, value, "nx", "ex", expireSeconds);
        });
        return StringUtils.isNotBlank(status) && "OK".equals(status);
    }

}

package com.demo.service.convert.mapper;

import com.demo.model.dto.InsurancePolicyOrderMainDTO;
import com.demo.model.entity.baoxian.InsurancePolicyOrderMainEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author DongXL
 * @Create 2018-07-02 20:12
 */
@Mapper(componentModel = "spring")
public interface InsurancePolicyOrderMainMapper {

    InsurancePolicyOrderMainMapper INSTANCE = Mappers.getMapper(InsurancePolicyOrderMainMapper.class);

    InsurancePolicyOrderMainEntity toEntity(InsurancePolicyOrderMainDTO dto);

    InsurancePolicyOrderMainDTO toDTO(InsurancePolicyOrderMainEntity entity);
}

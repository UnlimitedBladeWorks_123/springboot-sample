package com.demo.service.convert.mapper;

import com.demo.model.domain.SpringbootDemoMoreDO;
import com.demo.model.dto.SpringbootDemoDTO;
import com.demo.model.dto.SpringbootDemoMoreDTO;
import com.demo.model.entity.baoxian.SpringbootDemoEntity;
import com.demo.model.entity.tob.SpringbootDemoTobEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author DongXL
 * @Create 2018-06-29 21:55
 */
@Mapper(componentModel = "spring")
public interface SpringbootDemoMapper {
    SpringbootDemoMapper INSTANCE = Mappers.getMapper(SpringbootDemoMapper.class);

    SpringbootDemoEntity toEntity(SpringbootDemoDTO dto);

    SpringbootDemoTobEntity toTobEntity(SpringbootDemoDTO dto);

    SpringbootDemoDTO toDTO(SpringbootDemoEntity entity);

    SpringbootDemoDTO toDTO(SpringbootDemoTobEntity entity);

    List<SpringbootDemoDTO> toDTO(List<SpringbootDemoEntity> entityList);

    SpringbootDemoMoreDTO toMoreDTO(SpringbootDemoMoreDO domain);

    List<SpringbootDemoMoreDTO> toMoreDTO(List<SpringbootDemoMoreDO> domainList);

}

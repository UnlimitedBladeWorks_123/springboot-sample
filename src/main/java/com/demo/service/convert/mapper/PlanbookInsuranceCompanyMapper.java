package com.demo.service.convert.mapper;

import com.demo.model.dto.PlanbookInsuranceCompanyDTO;
import com.demo.model.entity.baoxian.PlanbookInsuranceCompanyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author DongXL
 * @Create 2018-07-02 20:12
 */
@Mapper(componentModel = "spring")
public interface PlanbookInsuranceCompanyMapper {

    PlanbookInsuranceCompanyMapper INSTANCE = Mappers.getMapper(PlanbookInsuranceCompanyMapper.class);

    PlanbookInsuranceCompanyEntity toEntity(PlanbookInsuranceCompanyDTO dto);

    PlanbookInsuranceCompanyDTO toDTO(PlanbookInsuranceCompanyEntity entity);
}

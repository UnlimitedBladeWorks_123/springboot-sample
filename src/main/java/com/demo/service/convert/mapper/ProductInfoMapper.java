package com.demo.service.convert.mapper;

import com.demo.model.domain.ProductInfoMoreDO;
import com.demo.model.dto.ProductInfoMoreDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author DongXL
 * @Create 2018-06-29 21:55
 */
@Mapper(componentModel = "spring")
public interface ProductInfoMapper {

    ProductInfoMapper INSTANCE = Mappers.getMapper(ProductInfoMapper.class);

    ProductInfoMoreDO toDO(ProductInfoMoreDTO dto);

    ProductInfoMoreDTO toDTO(ProductInfoMoreDO entity);

    List<ProductInfoMoreDTO> toDTO(List<ProductInfoMoreDO> entityList);

}

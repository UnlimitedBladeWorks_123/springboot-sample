package com.demo.repository.redis;

import com.demo.model.redis.EsperRecord;
import org.springframework.data.keyvalue.repository.KeyValueRepository;

public interface EsperRecordRepository extends KeyValueRepository<EsperRecord, Long> {


}

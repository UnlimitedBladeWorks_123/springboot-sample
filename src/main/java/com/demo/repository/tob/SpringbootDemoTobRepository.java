package com.demo.repository.tob;

import com.demo.model.entity.tob.SpringbootDemoTobEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author DongXL
 * @Create 2018-03-19 18:27
 */
public interface SpringbootDemoTobRepository extends JpaRepository<SpringbootDemoTobEntity, String> {


}

package com.demo.repository.mongo;

import com.demo.model.document.SpringBootDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SpringBootDocumentRepository extends MongoRepository<SpringBootDocument, String> {
}

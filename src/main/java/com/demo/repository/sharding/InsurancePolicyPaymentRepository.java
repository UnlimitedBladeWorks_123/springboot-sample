package com.demo.repository.sharding;

import com.demo.model.entity.baoxian.InsurancePolicyPaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author DongXL
 * @Create 2018-07-16 11:20
 */
public interface InsurancePolicyPaymentRepository extends JpaRepository<InsurancePolicyPaymentEntity, String> {
}

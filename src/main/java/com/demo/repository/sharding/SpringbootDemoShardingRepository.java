package com.demo.repository.sharding;

import com.demo.model.domain.SpringbootDemoMoreDO;
import com.demo.model.entity.baoxian.SpringbootDemoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Stream;

/**
 * @Author DongXL
 * @Create 2018-03-19 18:27
 */
public interface SpringbootDemoShardingRepository extends JpaRepository<SpringbootDemoEntity, String> {

    List<SpringbootDemoEntity> getAllByDeleted(Boolean deleted);

    @Query("select new  com.demo.model.bo.SpringbootDemoMore(a.id, a.cname,b.name) from SpringbootDemoEntity a,com.demo.model.entity.tob.SpringbootDemoTobEntity b where a.id = b.id")
    List<SpringbootDemoMoreDO> multiDataSourceSearch();

    Stream<SpringbootDemoEntity> streamAllBy();

    List<SpringbootDemoEntity> findAllByOrderNo(String orderNo);
}

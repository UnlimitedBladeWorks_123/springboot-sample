package com.demo.repository.sharding;

import com.demo.model.entity.baoxian.InsurancePolicyOrderMainEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @description: 分表insurance_policy_order_main对应的DAO类
 * @author: Anjie
 * @date: 2018-06-07 18:44
 **/
public interface InsurancePolicyOrderMainRepository extends JpaRepository<InsurancePolicyOrderMainEntity, String> {
	InsurancePolicyOrderMainEntity findByOrderNo(String orderNo);

}
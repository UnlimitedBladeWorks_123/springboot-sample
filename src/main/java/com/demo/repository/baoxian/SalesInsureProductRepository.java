package com.demo.repository.baoxian;

import com.demo.model.domain.ProductInfoMoreDO;
import com.demo.model.entity.baoxian.SalesInsureProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

/**
 * @Author DongXL
 * @Create 2017-12-15 18:28
 */
public interface SalesInsureProductRepository extends JpaRepository<SalesInsureProductEntity, Long> {

    /**
     * 需要考虑ProductInfoMore构造方法的入参顺序
     *
     * @param type
     * @return
     */
    @Query(value = "select new com.demo.model.domain.ProductInfoMoreDO (a.id,a.name, a.logoUrl, b.id, b.name,b.type) from PlanbookInsuranceCompanyEntity a, SalesInsureProductEntity b where a.id =b.companyId and b.type=?1")
    List<ProductInfoMoreDO> getProductInfoMoreList(Integer type);

    @Query(value = "select new map (a.id as companyId,a.name as companyName, a.logoUrl as companyLogoUrl, b.id as productId, b.name as productName,b.type as productType) from PlanbookInsuranceCompanyEntity a, SalesInsureProductEntity b where a.id =b.companyId and b.type=?1")
    List<Map<String, Object>> getProductInfoMoreListWithMap(Integer type);

    @Query(value = "select new com.demo.model.domain.ProductInfoMoreDO (a.id,a.name, a.logoUrl, b.id, b.name,b.type) from PlanbookInsuranceCompanyEntity a, SalesInsureProductEntity b where a.id =b.companyId and (?1 is null or b.type=?1)")
    List<ProductInfoMoreDO> getProductInfoMoreListWithDynamicParams(Integer type, Sort sort);

    @Query(value = "select new com.demo.model.domain.ProductInfoMoreDO(b.id,b.name, b.logoUrl, a.id, a.name,a.type) from SalesInsureProductEntity a, PlanbookInsuranceCompanyEntity b where b.id =a.companyId and (?1 is null or a.type=?1) ")
    List<ProductInfoMoreDO> getProductInfoMoreListWithDynamicParams(Integer type, Pageable pageable);

    @Query(value = "select count(*) from PlanbookInsuranceCompanyEntity a, SalesInsureProductEntity b where a.id =b.companyId and (?1 is null or b.type=?1) ")
    Integer countProductInfoMoreListWithDynamicParams(Integer type);

    @Query(value = "select new com.demo.model.domain.ProductInfoMoreDO(b.id,b.name, b.logoUrl, a.id, a.name,a.type) from SalesInsureProductEntity a, PlanbookInsuranceCompanyEntity b where b.id =a.companyId and (?1 is null or a.type=?1) ")
    Page<ProductInfoMoreDO> getProductInfoMoreListWithDynamicParams2(Integer type, Pageable pageable);

}

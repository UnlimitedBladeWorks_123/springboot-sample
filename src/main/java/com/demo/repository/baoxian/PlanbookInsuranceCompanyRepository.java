package com.demo.repository.baoxian;

import com.demo.model.entity.baoxian.PlanbookInsuranceCompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @Author DongXL
 * @Create 2017-12-15 18:28
 */
public interface PlanbookInsuranceCompanyRepository extends JpaRepository<PlanbookInsuranceCompanyEntity, Long>, JpaSpecificationExecutor<PlanbookInsuranceCompanyEntity> {


}

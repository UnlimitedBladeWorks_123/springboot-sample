package com.demo.model.entity.baoxian;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Setter
@Getter
@DynamicUpdate
@DynamicInsert
@Table(name = "INSURANCE_POLICY_PAYMENT")
public class InsurancePolicyPaymentEntity implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 3259074315112380828L;


    @Column(name = "uuid")
    private String uuid;

    @Id
    @Column(name = "ORDER_NO")
    private String orderNo;

    /** 支付标识 */
    @Column(name = "THIRD_PAY_ID")
    private String thirdPayId;

    /** 保费金额 */
    @Column(name = "PAY_PREMIUM")
    private BigDecimal payPremium;

    /** 支付时间 */
    @Column(name = "PAY_TIME")
    private Date payTime;

    /** 支付渠道 **/
    @Column(name = "PAY_CHANNEL")
    private Integer payChannel;

    /** 支付方式 ***/
    @Column(name = "PAY_TYPE")
	private Integer payType;

    /**
     * 支付状态 100-待付款  200-付款成功  300-订单关闭
     **/
    @Column(name = "STATUS")
    private Integer status;

    /** 首期缴费银行代码 */
    @Column(name = "INIT_PAY_BANK_CODE")
    private String initPayBankCode;

    /** 首期缴费银行名称 */
    @Column(name = "INIT_PAY_BANK_NAME")
    private String initPayBankName;

    /** 首期缴费账户类型 */
    @Column(name = "INIT_PAY_ACCT_TYPE")
    private String initPayAcctType;

    /** 首期缴费银行卡号 */
    @Column(name = "INIT_PAY_ACCT_NO")
    private String initPayAcctNo;

    /** 首期缴费信用卡有效期 */
    @Column(name = "INIT_PAY_CARD_EXPIRED_DATE")
    private String initPayCardExpiredDate;

    /** 首期缴费账户姓名 */
    @Column(name = "INIT_PAY_ACCT_NAME")
    private String initPayAcctName;

    /** 首期持卡人身份证号 */
    @Column(name = "INIT_PAY_ACCT_ID_NO")
    private String initPayAcctIdNo;

    /** 首期缴费银行卡账号预留手机号码 */
    @Column(name = "INIT_PAY_ACCT_MOBILE")
    private String initPayAcctMobile;

    @Column(name = "PAY_UUID")
    private String payUuid;

    @Column(name = "pay_url")
    private String payUrl;

    /**  */
    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    /**  */
    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

}
package com.demo.model.entity.baoxian;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Setter
@Getter
@DynamicUpdate
@DynamicInsert
@Table(name = "INSURANCE_POLICY_PERSON_BASE")
public class InsurancePolicyPersonBaseEntity implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 468626049115540253L;

    @Column(name = "uuid")
    private String uuid;

    /** 订单唯一标识 */
    @Id
    @Column(name = "ORDER_NO")
    private String orderNo;

    /** 姓名 */
    @Column(name = "NAME")
    private String name;

    /** 人员类型  1-投保人  2-被保人   3-受益人*/
    @Column(name = "TYPE")
    private Integer type;

    /** 性别 */
    @Column(name = "SEX")
    private Integer sex;

    /** 生日 */
    @Column(name = "BIRTHDAY")
    private Date birthday;

    /** 手机号码 */
    @Column(name = "MOBILE")
    private String mobile;

    /** id_type */
    @Column(name = "ID_TYPE")
    private Integer idType;

    /** 证件号码 */
    @Column(name = "ID_NO")
    private String idNo;

    /** 证件有效开始时间 */
    @Column(name = "ID_VALID_START_DATETIME")
    private Date idValidStartDatetime;

    /** 证件有效结束时间 */
    @Column(name = "ID_VALID_END_DATETIME")
    private Date idValidEndDatetime;

    /** 职业类别 */
    @Column(name = "JOB_TYPE")
    private String jobType;

    /** 职业编码 */
    @Column(name = "JOB_CODE")
    private String jobCode;

    /** 职业名称 */
    @Column(name = "JOB_NAME")
    private String jobName;

    /** 职业描述 */
    @Column(name = "JOB_DESC")
    private String jobDesc;

    /** 英文名 */
    @Column(name = "ENGLISH_NAME")
    private String englishName;

    /** 省份编码 */
    @Column(name = "PROVINCE_CODE")
    private String provinceCode;

    /** 市编码 */
    @Column(name = "CITY_CODE")
    private String cityCode;

    /** 区编码 */
    @Column(name = "DISTRICT_CODE")
    private String districtCode;

    /** 联系地址 */
    @Column(name = "ADDRESS")
    private String address;

    /** 国籍 */
    @Column(name = "NATIONALITY")
    private String nationality;

    /** 邮政编码 */
    @Column(name = "ZIP")
    private String zip;

    /** 工作单位 */
    @Column(name = "WORK_COMPANY")
    private String workCompany;

	/** 工作描述 */
	@Column(name = "WORK_DESC")
	private String workDesc;

    /** 身高 */
    @Column(name = "HEIGHT")
    private Integer height;

    /** 体重 */
    @Column(name = "WEIGHT")
    private Double weight;

	/**
	 * 婚姻状况
	 */
	@Column(name = "MARRIAGE")
    private Integer marriage;

    /** 收入类型 */
    @Column(name = "INCOME_TYPE")
    private Integer incomeType;

    /** 收入 */
    @Column(name = "INCOME")
    private BigDecimal income;


    /**  */
    @Column(name = "BENEFIT_TYPE")
    private Integer benefitType;

    /** 被保人与投保人关系 */
    @Column(name = "RELATION_TO_APPLICANT")
    private Integer relationToApplicant;

    /**  */
    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    /**  */
    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

}
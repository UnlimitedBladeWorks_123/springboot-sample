package com.demo.model.entity.baoxian;
/*
 * Welcome to use the TableGo Tools.
 *
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 *
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.0.0
 */

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * PLANBOOK_INSURANCE_COMPANY
 *
 * @author bianj
 * @version 1.0.0 2018-03-07
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Data
@Table(name = "PLANBOOK_INSURANCE_COMPANY")
public class PlanbookInsuranceCompanyEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Date createTime;
    private Date recordUpdateDatetime;
    private Boolean isDeleted;
    private String logoUrl;
    private Integer orderTag;
    private String logoColor;
    private String fullName;
    private String cardName;
    private Boolean hasPlanbook;
    private Boolean hasClause;
    private String code;
    private String wechatLogoUrl;
    private Boolean isGroup;
    private String claimPhone;
    private Long insureZrxId;
    private Long insureProdunctId;
    /**
     * 色值
     */
    private String colour;
    /**
     * 大数据部门使用作为固定公司名的标签，请不要修改
     */
    private String reportName;

}
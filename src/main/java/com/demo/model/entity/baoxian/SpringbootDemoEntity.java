package com.demo.model.entity.baoxian;

import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * CREATE TABLE `springboot_demo` (
 * `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
 * `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除，0：否 1：是',
 * `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 * `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
 * `cname` varchar(255) DEFAULT NULL COMMENT '名称',
 * PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='测试表';
 *
 * @Author DongXL
 * @Create 2018-03-19 15:15
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Data
@Table(name = "SPRINGBOOT_DEMO")
@SQLDelete(sql = "update springboot_demo set deleted =1 where id =?")
@Where(clause = "deleted = 0")
public class SpringbootDemoEntity implements Serializable {

    @Id
    private Long id;
    private Date createTime;
    private Date updateTime;
    private String cname;
    private Boolean deleted;
    private String orderNo;
}

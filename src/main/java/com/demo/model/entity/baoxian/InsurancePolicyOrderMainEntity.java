package com.demo.model.entity.baoxian;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Setter
@Getter
@DynamicUpdate
@DynamicInsert
@Table(name = "INSURANCE_POLICY_ORDER_MAIN")
public class InsurancePolicyOrderMainEntity implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -991852385541398211L;

    /** 保单唯一标识 */
    @Column(name = "POLICY_UUID")
    private String policyUuid;

    /** 订单唯一标识 */
    @Id
    @Column(name = "ORDER_NO")
    private String orderNo;

    /** 1-赠险   2-个险  3-互助保险  6-长险 */
    @Column(name = "TYPE")
    private Integer type;

    /** 产品ID */
    @Column(name = "PRODUCT_ID")
    private Long productId;

    /** 投保单号 */
    @Column(name = "PROPOSAL_NO")
    private String proposalNo;

    /** 保单号 */
    @Column(name = "POLICY_NO")
    private String policyNo;

    /** 代理人ID */
    @Column(name = "USER_ID")
    private Long userId;

    /** 被保人数量 */
    @Column(name = "INSURED_NUM")
    private Integer insuredNum;

	/** 计划code **/
	@Column(name = "PLAN_CODE")
	private Integer planCode;


    /** 订单状态：55-待用户支付  300-核保失败  200-出单成功  600-订单关闭  700-保单退保  800-保单撤单 */
    @Column(name = "STATUS")
    private Integer status;

    /** 失败信息 */
    @Column(name = "FAIL_MSG")
    private String failMsg;

    /** 总保费 */
    @Column(name = "TOTAL_PREMIUM")
    private BigDecimal totalPremium;

    /** 推广费 */
    @Column(name = "BONUS")
    private BigDecimal bonus;

    /** 关闭时间 */
    @Column(name = "OFF_TIME")
    private Date offTime;

    /** 出单时间 */
    @Column(name = "SUCCESS_TIME")
    private Date successTime;

    /**  */
    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    /**  */
    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

}
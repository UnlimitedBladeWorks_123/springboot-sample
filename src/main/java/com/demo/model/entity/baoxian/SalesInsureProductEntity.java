package com.demo.model.entity.baoxian;/*
 * Welcome to use the TableGo Tools.
 *
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 *
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.0.0
 */

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * SALES_INSURE_PRODUCT
 *
 * @author bianj
 * @version 1.0.0 2018-03-07
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Data
@Table(name = "SALES_INSURE_PRODUCT")
public class SalesInsureProductEntity implements Serializable {

    /**  */
    @Id
    @GeneratedValue
    private Long id;
    /**  */
    private Long companyId;
    /**  */
    private String companyName;
    /**  */
    private String name;
    /**  */
    private Integer type;
    /** 指向sales_insure_product_url的id,用于app端商城页面和订单页面跳转 */
    private Integer urlId;
    /**  */
    private Date recordUpdateDatetime;
    /**  */
    private Boolean isDeleted;
    /**  */
    private Boolean isEnable;
    /**  */
    private Integer orderTag;
    /**  */
    private String descJson;
    /**  */
    private String imgUrl;
    /**  */
    private Double baoe;
    /**  */
    private Integer age;
    /**  */
    private String companyLogo;
    /**  */
    private String adsStr;
    /**  */
    private Double price;
    /** 增险 交易用户优惠价格 */
    private Double tradeUserPrice;
    /**  */
    private Integer showType;
    /**  */
    private String detailLogo;
    /**  */
    private String code;
    /**  */
    private Integer sellCount;
    /**  */
    private Long startDatetime;
    /**  */
    private Long endDatetime;
    /**  */
    private Integer modifyDatetime;
    /**  */
    private Integer createDatetime;
    /**  */
    private String validTimeType;
    /**  */
    private Long validDays;
    /**  */
    private Long validStartDatetime;
    /**  */
    private Long validEndDatetime;
    /**  */
    private String infoJson;
    /** 所属首页产品catogry */
    private Long categoryId;
    /** 总数量 */
    private Integer totalCount;
    /** 原价 */
    private Double originalCost;
    /**  */
    @Column(name = "promotion_img_s")
    private String promotionImgS;
    /**  */
    private Boolean isPromotion;
    /**  */
    private String promotionImg;
    /**  */
    private String claimPdfUrl;
    /** 推广产品截止时间或剩余份数标示 0：不显示、 1：开卖时间倒计时、2：剩余时间倒计时、3：剩余份数 */
    private Integer promotionType;
    /**  */
    private String statusCode;
    /**  */
    private String descr;
    /** app显示用price */
    private String showPrice;
    /** 首页列表说明 */
    private String subInfo;
    /**  */
    private String unit;
    /** 所属小分类id */
    private Long subCategoryId;
    /** 所属签约公司 */
    private Long belongCompany;
    /**  */
    private String orderUrl;
    /**  */
    private String adsTagStr;
    /**  */
    private Integer effectDays;
    /**  */
    private String profitDesc;
    /**  */
    private Boolean hasFeature;
    private String jumpTitle;
    /** 跳转图片对应的URL */
    private String jumpImgUrl;
    /** 赠险跳转弹框描述 */
    private String jumpDesc;
    /** 车险公司支付页面 */
    private String carPayUrl;
    /** 存templateinfo */
    private String propertyJson;
    /**  */
    private String homeBannerLogo;
    /** 去赚钱版块产品展示图片  */
    private String earnImgUrl;
    /** 去赚钱版块公司logo */
    private String earnCompanyLogo;
    /**  */
    private String area;
    /** 1、前后端分离 */
    private Boolean isNewProduct;
    /** 1、模板化产品。2、新接口化产品过渡 */
    private Integer isTemplate;
    /** 是否热卖产品,0:不是,1:是 */
    private Integer isHot;
    /** 是否爆款,0:不是,1:是 */
    private Integer isFiery;
    /** 爆款排序(倒序) */
    private Long fieryOrder;
    /** 显示渠道,默认0 */
    private Integer displayChannel;
    /** 预热产品 */
    private Integer isPreheat;
    /** vip_advertisement */
    private String vipAdvertisement;
    /** vip广告链接配置 */
    private String vipAdvertisementUrl;
    /**  */
    @Column(name="company_logo_v2")
    private String companyLogoV2;
    /** 理赔类型:1协助理赔;2极速理赔;0不支持理赔 */
    private Integer claimType;
    /** 1.直营商城展示 */
    private Integer insuranceStore;
    /** 标签 */
    private String tagStr;
    /** 直营店销量 */
    private Integer storeSellCount;
    /** 是否支持批量出单 0-false，1-true */
    private Integer batchInsureSupport;
    /** 0,平台支付, 1,第三方支付 */
    private Integer thirdPartyPay;
    /** 0 待支付订单超时关闭不需要调用撤销核保接口，1 需要 */
    private Integer checkPolicyRevoke;
    /** 自定义标签 */
    private String customTag;
    /** 是否显示服务信息 */
    private Integer showServiceInformation;
    /** 是否显示问答版块 */
    private Integer showQuestion;
    /** 续保产品 */
    private Integer renewalType;
    /** 产品特色文案 */
    private String featureDesc;
    /** 直营店产品 上下架 0 下架  1 上架 */
    private Boolean storeEnableFlag;
    /** 产品犹豫期(按天计算) */
    private Integer hesitationPhase;
    /**  */
    private Boolean renewalWithholdFlag;
    /** 灰度图地址 */
    private String companyLogoDisabled;
    /** 长险列表推广费文案 */
    private String showBonus;
    /** 长险佣金表 */
    private String commissionTable;
    /** 长险续期策略文案 */
    private String renewalStrategy;
    /**  */
    private String withholdNotes;
    /** 0:不支持撤单, 1:支持 */
    private Integer supportRefund;
    /** 0:保险公司退款,1:我们公司退款 */
    private Integer refundType;
    /** 是否在列表页显示销售助手：1：长险销售助手... */
    private Integer salesAssistant;
    /** 产品code */
    private String productCode;
    /** 非交易用户是否展示领取红包页面0不展示 1展示 */
    private Integer redPacketShow;
    /**  */
    private Date createTime;
    /**  */
    private Boolean cancelRenewalWithholdFlag;


}
package com.demo.model.entity.tob;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * CREATE TABLE `springboot_demo` (
 * `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
 * `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除，0：否 1：是',
 * `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 * `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
 * `cname` varchar(255) DEFAULT NULL COMMENT '名称',
 * PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='测试表';
 *
 * @Author DongXL
 * @Create 2018-03-19 15:15
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Data
@Table(name = "SPRINGBOOT_DEMO_TOB", catalog = "baoxian2b")
public class SpringbootDemoTobEntity implements Serializable {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Id
    @GeneratedValue(generator = "myLong")
    @GenericGenerator(name = "myLong", strategy = "assigned")
    private String name;
}

package com.demo.model.bo;

/**
 * Created by Administrator on 2017/9/5.
 */
public class EpolicyEmailSendResult {

    private Boolean succeed;
    private String failedMessage;

    public Boolean isSucceed() {
        return succeed;
    }

    public void setSucceed(Boolean succeed) {
        this.succeed = succeed;
    }

    public String getFailedMessage() {
        return failedMessage;
    }

    public void setFailedMessage(String failedMessage) {
        this.failedMessage = failedMessage;
    }
}

package com.demo.model.exceptions;


import com.demo.model.enums.BusinessErrorEnum;

/**
 * @Author DongXL
 * @Create 2018-03-26 15:00
 */
public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(BusinessErrorEnum businessError) {
        super(businessError.getMessage());
    }


}

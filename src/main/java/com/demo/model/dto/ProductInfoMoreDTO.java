package com.demo.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author DongXL
 * @Create 2018-03-14 12:55
 */
@Data
public class ProductInfoMoreDTO implements Serializable {

    private Long companyId;
    private String companyName;
    private String companyLogoUrl;
    private Long productId;
    private String productName;
    private Integer productType;

}

package com.demo.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author DongXL
 * @Create 2018-06-29 20:00
 */
@Data
public class SpringbootDemoDTO implements Serializable {

    private Long id;
    private Date createTime;
    private Date updateTime;
    private String cname;
    private String name;
    private Boolean deleted;
    private String orderNo;
}

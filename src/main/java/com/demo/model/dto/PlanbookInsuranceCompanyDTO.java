package com.demo.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author DongXL
 * @Create 2018-07-02 20:12
 */

@Data
public class PlanbookInsuranceCompanyDTO implements Serializable {

    private Long id;
    private String name;
    private Date createTime;
    private Date recordUpdateDatetime;
    private Boolean isDeleted;
    private String logoUrl;
    private Integer orderTag;
    private String logoColor;
    private String fullName;
    private String cardName;
    private Boolean hasPlanbook;
    private Boolean hasClause;
    private String code;
    private String wechatLogoUrl;
    private Boolean isGroup;
    private String claimPhone;
    private Long insureZrxId;
    private Long insureProdunctId;
    /**
     * 色值
     */
    private String colour;
    /**
     * 大数据部门使用作为固定公司名的标签，请不要修改
     */
    private String reportName;

}
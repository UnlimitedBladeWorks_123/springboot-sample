package com.demo.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;


@Data
public class InsurancePolicyOrderMainDTO implements Serializable {

    private Long id;

    /**
     * 保单唯一标识
     */
    private String policyUuid;

    /**
     * 订单唯一标识
     */
    private String orderNo;

    /**
     * 1-赠险   2-个险  3-互助保险  6-长险
     */
    private Integer type;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 投保单号
     */
    private String proposalNo;

    /**
     * 保单号
     */
    private String policyNo;

    /**
     * 代理人ID
     */
    private Long userId;

    /**
     * 被保人数量
     */
    private Integer insuredNum;

    /**
     * 计划code
     **/
    private Integer planCode;


    /**
     * 订单状态：55-待用户支付  300-核保失败  200-出单成功  600-订单关闭  700-保单退保  800-保单撤单
     */
    private Integer status;

    /**
     * 失败信息
     */
    private String failMsg;

    /**
     * 总保费
     */
    private BigDecimal totalPremium;

    /**
     * 推广费
     */
    private BigDecimal bonus;

    /**
     * 关闭时间
     */
    private Date offTime;

    /**
     * 出单时间
     */
    private Date successTime;

    /**  */
    private Timestamp createTime;

    /**  */
    private Timestamp updateTime;

}
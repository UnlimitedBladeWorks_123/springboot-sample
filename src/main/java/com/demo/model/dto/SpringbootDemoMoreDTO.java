package com.demo.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author DongXL
 * @Create 2018-03-29 19:57
 */
@Data
public class SpringbootDemoMoreDTO implements Serializable {
    private Long id;
    private String cname;
    private String tobName;
}

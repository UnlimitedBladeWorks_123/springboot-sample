package com.demo.model.dto;

import com.demo.esper.event.AbstractEsperEvent;
import lombok.Data;

/**
 *
 */
@Data
public class DemoEsperEvent extends AbstractEsperEvent {

    /**
     * 年龄、子女年龄（计算教育金）
     */
    private Integer age;

    /**
     * 年收入
     * 单位  元/年
     */
    private Double income;

    /**
     * 年支出
     * 单位  元/年
     */
    private Double outgo;

    /**
     * 人寿目标保额
     */
    private Double lifeTargetAmount;

    /**
     * 其他债务
     */
    private Double otherDebt;

    /**
     * 房贷
     * 单位  元
     */
    private Double houseLeftMortgage;

    /**
     * 车贷
     * 单位  元
     */
    private Double carLeftMortgage;

    /**
     * 每月养老支出
     * 单位  元
     */
    private Double retireOutgoPerMonth;


}

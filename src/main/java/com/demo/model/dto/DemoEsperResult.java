package com.demo.model.dto;

import com.demo.esper.result.EsperResult;
import lombok.Data;

@Data
public class DemoEsperResult implements EsperResult {

    private Double targetAmount;
}
package com.demo.model.enums;

/**
 * @Author DongXL
 * @Create 2018-03-26 15:01
 */
public enum BusinessErrorEnum {

    COMMON_DATA_EXISTS("数据已存在"),
    PRODUCT_CONFIG_PRODUCT_CODE_EXISTS("该产品配置已存在");

    private String message;

    BusinessErrorEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}

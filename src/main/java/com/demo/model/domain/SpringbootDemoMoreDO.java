package com.demo.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author DongXL
 * @Create 2018-03-29 19:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SpringbootDemoMoreDO implements Serializable {
    private Long id;
    private String cname;
    private String tobName;
}

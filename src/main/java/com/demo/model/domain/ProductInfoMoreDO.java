package com.demo.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author DongXL
 * @Create 2018-03-14 12:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfoMoreDO implements Serializable {

    private Long companyId;
    private String companyName;
    private String companyLogoUrl;
    private Long productId;
    private String productName;
    private Integer productType;

}

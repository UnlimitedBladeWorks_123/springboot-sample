package com.demo.model.common;

public class DemoConstant {

    public static final String DEMO_EVENT_TYPE_NAME = "data";
    public static final String RULE_GROUP_NAME = "demoTest";

}

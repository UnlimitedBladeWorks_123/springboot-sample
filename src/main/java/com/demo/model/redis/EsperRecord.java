package com.demo.model.redis;

import com.demo.esper.event.AbstractEsperEvent;
import com.demo.esper.result.EsperResult;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.util.Date;
import java.util.Map;

/**
 * timeToLive 60分钟存活时间
 */
@RedisHash(value = "esperRecords", timeToLive = 15 * 60)
@Data
public class EsperRecord {
    @Id
    private Long eventId;
    private String ruleGroupName;
    private Date createTime;
    private AbstractEsperEvent event;
    private Map<String, Object> result;
}

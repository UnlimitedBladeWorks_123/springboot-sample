package com.demo.uitls;

import java.math.BigDecimal;

public enum NumberUtils {

    INSTANCE;

    public Double scaleRoundHalfUp(Double value, int scale) {
        if (value == null) {
            return null;
        }
        BigDecimal bd = new BigDecimal(value).setScale(scale, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }
}

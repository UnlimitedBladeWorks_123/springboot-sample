package com.demo.uitls;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum VersionCompareUtils {

    INSTANCE;

    public boolean isLower(String baseVersion, String compareVersion) {
        try {
            if (StringUtils.isNotBlank(baseVersion) && StringUtils.isNotBlank(compareVersion)) {
                int[] baseArr = Arrays.stream(StringUtils.split(baseVersion, ".")).mapToInt(o -> Integer.parseInt(o)).toArray();
                int[] compareArr = Arrays.stream(StringUtils.split(compareVersion, ".")).mapToInt(o -> Integer.parseInt(o)).toArray();
                int len = Math.min(baseArr.length, compareArr.length);
                for (int i = 0; i < len; i++) {
                    if (compareArr[i] < baseArr[i]) {
                        return true;
                    }
                    if (baseArr.length > len) {
                        for (int j = len; j < baseArr.length; j++) {
                            if (baseArr[j] > 0) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

}

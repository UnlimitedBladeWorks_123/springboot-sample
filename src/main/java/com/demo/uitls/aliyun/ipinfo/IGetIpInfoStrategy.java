package com.demo.uitls.aliyun.ipinfo;

/**
 * @Author DongXL
 * @Create 2017-03-27 17:20
 */
public interface IGetIpInfoStrategy {

    IpInfo getIpInfo(String ip) throws Exception;
}

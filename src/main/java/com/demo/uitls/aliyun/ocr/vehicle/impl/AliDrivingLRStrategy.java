package com.demo.uitls.aliyun.ocr.vehicle.impl;

import com.alibaba.fastjson.JSON;
import com.demo.uitls.aliyun.ocr.abs.AbstractAliIRStrategy;
import com.demo.uitls.aliyun.ocr.vehicle.IDrivingLRStrategy;
import com.demo.uitls.aliyun.ocr.vehicle.dto.DrivingLicenseInfo;

/**
 * @Author DongXL
 * @Create 2016-11-16 17:44
 */
public class AliDrivingLRStrategy extends AbstractAliIRStrategy implements IDrivingLRStrategy {

    public static final AliDrivingLRStrategy INSTANCE = new AliDrivingLRStrategy();

    private static final String HOST = "dm-53.data.aliyun.com";

    private static final String PATH = "/rest/160601/ocr/ocr_vehicle.json";

    @Override
    public DrivingLicenseInfo recognize(byte[] bytes) throws Exception {
        return (DrivingLicenseInfo) super.recognize(bytes);
    }

    @Override
    public String returnInterfaceHost() {
        return HOST;
    }

    @Override
    public String returnInterfacePath() {
        return PATH;
    }

    @Override
    public Object returnObj(String data) {
        return JSON.parseObject(data, DrivingLicenseInfo.class);
    }

}

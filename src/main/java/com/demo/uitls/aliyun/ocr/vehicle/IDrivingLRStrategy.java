package com.demo.uitls.aliyun.ocr.vehicle;

import com.demo.uitls.aliyun.ocr.vehicle.dto.DrivingLicenseInfo;

/**
 * @Author DongXL
 * @Create 2016-11-16 17:26
 * Driving License Recognize Interface
 */
public interface IDrivingLRStrategy {

    DrivingLicenseInfo recognize(byte[] bytes) throws Exception;

}

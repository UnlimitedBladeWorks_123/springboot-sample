package com.demo.uitls.aliyun.ocr.idcard.dto;

import java.io.Serializable;

/**
 * @Author DongXL
 * @Create 2016-11-16 17:31
 */
public class IDCardInfo implements Serializable {

    private String name;
    private String address;
    private String num;
    private String sex;
    private String birth;
    private String nationality;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}

package com.demo.uitls.aliyun.ocr.busilicence.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author DongXL
 * @Create 2017-03-21 17:09
 */
@Data
public class BusinessLicenceInfo implements Serializable {

    @JSONField(name = "reg_num")
    private String regNum;
    private String name;
    private String person;
    @JSONField(name = "valid_period")
    private String validPeriod;
    private String address;

}

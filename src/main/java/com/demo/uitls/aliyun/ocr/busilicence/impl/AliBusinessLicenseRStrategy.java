package com.demo.uitls.aliyun.ocr.busilicence.impl;

import com.alibaba.fastjson.JSON;
import com.demo.uitls.aliyun.ocr.abs.AbstractAliIRStrategy;
import com.demo.uitls.aliyun.ocr.busilicence.IBusinessLicenseRStategy;
import com.demo.uitls.aliyun.ocr.busilicence.dto.BusinessLicenceInfo;

/**
 * @Author DongXL
 * @Create 2017-03-21 20:45
 */
public class AliBusinessLicenseRStrategy extends AbstractAliIRStrategy implements IBusinessLicenseRStategy {

    public static final AliBusinessLicenseRStrategy INSTANCE = new AliBusinessLicenseRStrategy();

    private final static String HOST = "dm-58.data.aliyun.com";
    private final static String PATH = "/rest/160601/ocr/ocr_business_license.json";

    @Override
    public BusinessLicenceInfo recognize(byte[] bytes) throws Exception {
        return (BusinessLicenceInfo) super.recognize(bytes);
    }


    @Override
    public String returnInterfaceHost() {
        return HOST;
    }

    @Override
    public String returnInterfacePath() {
        return PATH;
    }

    @Override
    public Object returnObj(String data) {
        return JSON.parseObject(data, BusinessLicenceInfo.class);
    }


}

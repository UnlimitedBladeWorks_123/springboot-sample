package com.demo.uitls.aliyun.ocr.vehicle.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author DongXL
 * @Create 2016-11-16 17:31
 */
@Data
public class DrivingLicenseInfo implements Serializable {
    @JSONField(name = "engine_num")
    private String engineNum;//发动机号码
    @JSONField(name = "issueDate")
    private String issue_date;
    @JSONField(name = "config_str")
    private String configStr;
    private String model;//品牌型号
    private String owner;
    @JSONField(name = "plate_num")
    private String plateNum;//车牌
    @JSONField(name = "register_date")
    private String registerDate;
    private String success;
    @JSONField(name = "vehicle_type")
    private String vehicleType;
    private String vin;//车辆识别代码

}

package com.demo.uitls.aliyun.ocr.idcard.impl;

import com.alibaba.fastjson.JSON;
import com.demo.uitls.aliyun.ocr.abs.AbstractAliIRStrategy;
import com.demo.uitls.aliyun.ocr.idcard.IIDCardRStrategy;
import com.demo.uitls.aliyun.ocr.idcard.dto.IDCardInfo;

/**
 * Created by Administrator on 2016/8/23.
 */
public class AliIDCardRStrategy extends AbstractAliIRStrategy implements IIDCardRStrategy {

    public static final AliIDCardRStrategy INSTANCE = new AliIDCardRStrategy();

    private final static String HOST = "dm-51.data.aliyun.com";

    private final static String PATH = "/rest/160601/ocr/ocr_idcard.json";


    @Override
    public IDCardInfo recognize(byte[] bytes) throws Exception {
        return (IDCardInfo) super.recognize(bytes);
    }

    @Override
    public String returnInterfaceHost() {
        return HOST;
    }

    @Override
    public String returnInterfacePath() {
        return PATH;
    }

    @Override
    public Object returnObj(String data) {
        return JSON.parseObject(data, IDCardInfo.class);
    }

}

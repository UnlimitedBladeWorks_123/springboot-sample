package com.demo.uitls.aliyun.ocr.abs;

import com.alibaba.fastjson.JSON;
import com.demo.uitls.aliyun.common.AbstractAliCallTemplate;
import com.demo.uitls.aliyun.common.Request;
import com.demo.uitls.aliyun.common.Response;
import com.demo.uitls.aliyun.common.constant.Constants;
import com.demo.uitls.aliyun.common.constant.ContentType;
import com.demo.uitls.aliyun.common.constant.HttpHeader;
import com.demo.uitls.aliyun.common.enums.Method;
import com.demo.uitls.aliyun.common.util.MessageDigestUtil;
import com.demo.uitls.aliyun.ocr.abs.dto.AliRParam;
import org.apache.commons.codec.binary.Base64;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author DongXL
 * @Create 2017-03-21 17:34
 */
public abstract class AbstractAliIRStrategy extends AbstractAliCallTemplate {

    private final String BODY_STRING = "imageBytes";

    public Object recognize(byte[] bytes) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put(BODY_STRING, buildBodyString(bytes));
        Response response = super.execute(params);
        String data = (JSON.parseObject(response.getBody()).getJSONArray("outputs")).getJSONObject(0).getJSONObject("outputValue").getString("dataValue");
        return returnObj(data);
    }

    private String buildBodyString(byte[] bytes) {
        AliRParam param = new AliRParam();
        List<AliRParam.InputsBean> beanList = new ArrayList<>();
        AliRParam.InputsBean inputsBean = new AliRParam.InputsBean();
        AliRParam.InputsBean.ImageBean imageBean = new AliRParam.InputsBean.ImageBean();
        AliRParam.InputsBean.ConfigureBean configureBean = new AliRParam.InputsBean.ConfigureBean();
        imageBean.setDataType(50);
        imageBean.setDataValue(Base64.encodeBase64String(bytes));
        configureBean.setDataType(50);
        configureBean.setDataValue("{\"side\":\"face\"}");
        inputsBean.setConfigure(configureBean);
        inputsBean.setImage(imageBean);
        beanList.add(inputsBean);
        param.setInputs(beanList);
        return JSON.toJSONString(param);
    }

    @Override
    public Method returnMethod() {
        return Method.POST_STRING;
    }

    @Override
    public void wrapQuerys(Request request, Map<String, String> params) {

    }

    @Override
    public void wrapHeaders(Request request, Map<String, String> params) {
        String bodyString = params.get(BODY_STRING);
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeader.HTTP_HEADER_USER_AGENT, Constants.USER_AGENT);
        headers.put(HttpHeader.HTTP_HEADER_ACCEPT, "application/json");
        headers.put(HttpHeader.HTTP_HEADER_CONTENT_MD5, MessageDigestUtil.base64AndMD5(bodyString));
        headers.put(HttpHeader.HTTP_HEADER_CONTENT_TYPE, ContentType.CONTENT_TYPE_TEXT);
        headers.put(HttpHeader.HTTP_HEADER_AUTHORIZATION, getUseAppCode());
        request.setHeaders(headers);
    }

    @Override
    public void wrapBody(Request request, Map<String, String> params) {
        String bodyString = params.get(BODY_STRING);
        request.setStringBody(bodyString);
    }


    public abstract Object returnObj(String data);


}

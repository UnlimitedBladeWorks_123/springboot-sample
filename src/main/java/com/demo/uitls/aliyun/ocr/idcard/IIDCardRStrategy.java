package com.demo.uitls.aliyun.ocr.idcard;

import com.demo.uitls.aliyun.ocr.idcard.dto.IDCardInfo;

/**
 * @Author DongXL
 * @Create 2016-11-16 17:26
 * Driving License Recognize Interface
 */
public interface IIDCardRStrategy {

    IDCardInfo recognize(byte[] bytes) throws Exception;

}

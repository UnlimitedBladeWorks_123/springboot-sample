package com.demo.uitls.aliyun.ocr.busilicence;

import com.demo.uitls.aliyun.ocr.busilicence.dto.BusinessLicenceInfo;

/**
 * @Author DongXL
 * @Create 2017-03-21 17:07
 */
public interface IBusinessLicenseRStategy {

    BusinessLicenceInfo recognize(byte[] bytes) throws Exception;

}

package com.demo.uitls.aliyun.common;

import com.demo.uitls.aliyun.common.constant.Constants;
import com.demo.uitls.aliyun.common.constant.HttpSchema;
import com.demo.uitls.aliyun.common.enums.Method;

import java.util.ArrayList;
import java.util.Map;

/**
 * @Author DongXL
 * @Create 2017-03-28 16:43
 */
public abstract class AbstractAliCallTemplate {
    private final String DEFAULT_APPKEY = "23711053";
    private final String DEFAULT_APPSECRET = "8b8b728482018723813b360d5312bb84";

    private final String DEFAULT_APPCODE = "daec386741d74747adb6491bea9493ed";
    // APP KEY
    private String appKey = DEFAULT_APPKEY;
    // APP密钥
    private String appSecret = DEFAULT_APPSECRET;
    // AppCode
    private String appCode = DEFAULT_APPCODE;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getUseAppCode() {
        return Constants.ALI_APP_CODE + appCode;
    }

    public Response execute(Map<String, String> params) throws Exception {
        String host = returnInterfaceHost();
        String path = returnInterfacePath();
        Request request = new Request(returnMethod(), HttpSchema.HTTPS + host, path, appKey, appSecret, Constants.DEFAULT_TIMEOUT);
        request.setSignHeaderPrefixList(new ArrayList<>());
        wrapQuerys(request, params);
        wrapHeaders(request, params);
        wrapBody(request, params);
        return Client.execute(request);
    }

    public abstract Method returnMethod();

    public abstract String returnInterfaceHost();

    public abstract String returnInterfacePath();

    public abstract void wrapQuerys(Request request, Map<String, String> params);

    public abstract void wrapHeaders(Request request, Map<String, String> params);

    public abstract void wrapBody(Request request, Map<String, String> params);
}

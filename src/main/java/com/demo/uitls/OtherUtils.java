package com.demo.uitls;

/**
 * 无具体功能
 *
 * @Author DongXL
 * @Create 2018-03-07 18:09
 */
public interface OtherUtils {

    /**
     * 时间工具类
     * @see org.apache.commons.lang3.time.DateUtils
     * @see org.apache.commons.lang3.time.DateFormatUtils
     *
     * 字符串工具类
     * @see org.apache.commons.lang3.StringUtils
     *
     * 加密
     * @see javax.crypto.Cipher
     * @see org.apache.commons.codec.digest.DigestUtils
     * @see org.apache.commons.codec.digest.HmacUtils
     *
     * Collection工具类
     * @see org.apache.commons.collections.CollectionUtils
     *
     * Map工具类
     * @see org.apache.commons.collections.MapUtils
     *
     * 文件工具类
     * @see org.apache.commons.io.FileUtils
     *
     * URL工具类
     * @see org.apache.http.client.utils.URLEncodedUtils
     *
     * Bean工具类
     * @see org.apache.commons.beanutils.BeanUtils
     *
     * 系统工具类
     * @see org.apache.commons.lang3.SystemUtils
     *
     *
     */

}

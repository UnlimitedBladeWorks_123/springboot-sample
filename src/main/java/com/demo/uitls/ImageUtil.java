package com.demo.uitls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

/**
 * Created by Administrator on 2015/3/23.
 */
public enum ImageUtil {
    INSTANCE;

    private static final Logger log = LoggerFactory.getLogger(ImageUtil.class);

    public String generateImage(byte[] b, String savePath, String filename) {
        //对字节数组字符串进行Base64解码并生成图片
        try (OutputStream out = new FileOutputStream(savePath + "/" + filename)) {
            //生成jpeg图片
            if (!new File(savePath).exists()) {
                new File(savePath).mkdirs();
            }
            out.write(b);
            out.flush();
            return savePath + "/" + filename;
        } catch (IOException e) {
            log.error("IO异常", e);
            return null;
        }
    }

    public BufferedImage rotate(Image src, int angel) {
        int srcWidth = src.getWidth(null);
        int srcHeight = src.getHeight(null);
        // calculate the new image size
        Rectangle rectDes = calcRotatedSize(new Rectangle(new Dimension(
                srcWidth, srcHeight)), angel);

        BufferedImage res = null;
        res = new BufferedImage(rectDes.width, rectDes.height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = res.createGraphics();
        // transform
        g2.translate((rectDes.width - srcWidth) / 2,
                (rectDes.height - srcHeight) / 2);
        g2.rotate(Math.toRadians(angel), srcWidth / 2.0, srcHeight / 2.0);

        g2.drawImage(src, null, null);
        return res;
    }

    public Rectangle calcRotatedSize(Rectangle src, int angel) {
        // if angel is greater than 90 degree, we need to do some conversion
        if (angel >= 90) {
            if (angel / 90 % 2 == 1) {
                int temp = src.height;
                src.height = src.width;
                src.width = temp;
            }
            angel = angel % 90;
        }

        double r = Math.sqrt(src.height * src.height * 1.0 + src.width * src.width) / 2.0;
        double len = 2 * Math.sin(Math.toRadians(angel) / 2.0) * r;
        double angelAlpha = (Math.PI - Math.toRadians(angel)) / 2.0;
        double angelDeltaWidth = Math.atan((double) src.height / src.width);
        double angelDeltaHeight = Math.atan((double) src.width / src.height);

        int lenDeltaWidth = (int) (len * Math.cos(Math.PI - angelAlpha
                - angelDeltaWidth));
        int lenDeltaHeight = (int) (len * Math.cos(Math.PI - angelAlpha
                - angelDeltaHeight));
        int desWidth = src.width + lenDeltaWidth * 2;
        int desHeight = src.height + lenDeltaHeight * 2;
        return new Rectangle(new Dimension(desWidth, desHeight));
    }

    public byte[] getBytes(String strUrl) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            URL u = new URL(strUrl);
            BufferedImage image = ImageIO.read(u);
            //convert BufferedImage to byte array
            ImageIO.write(image, "jpg", baos);
            baos.flush();
            return baos.toByteArray();
        } catch (Exception e) {
            log.error("IO异常", e);
        }
        return new byte[]{};
    }

}

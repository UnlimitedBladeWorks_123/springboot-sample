package com.demo.uitls;


/**
 * @Author DongXL
 * @Create 2018-05-11 15:43
 */
public enum SimHashUtils {

    INSTANCE;

    public SimHash getSimHash(String tokens) {
        SimHash simHash = new SimHash(tokens);
        return simHash;
    }

    public int calHammingDistance(SimHash hash1, SimHash hash2) {
        return hash1.hammingDistance(hash2);
    }

    public int calHammingDistance(String hashStr1, String hashStr2) {
        return SimHash.getDistance(hashStr1, hashStr2);
    }

}

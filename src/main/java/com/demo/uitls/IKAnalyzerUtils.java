package com.demo.uitls;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author DongXL
 * @Create 2018-05-31 9:47
 */
public enum IKAnalyzerUtils {
    INSTANCE;

    private static final Logger log = LoggerFactory.getLogger(IKAnalyzerUtils.class);

    public List<Word> getSplitWordList(String str) {
        List<Word> wordList = new ArrayList<>();
        // 构建IK分词器，使用smart分词模式
        Analyzer analyzer = new IKAnalyzer(true);
        // 获取Lucene的TokenStream对象
        TokenStream ts = null;
        try {
            ts = analyzer.tokenStream("myfield", new StringReader(str));
            // 获取词元位置属性
            OffsetAttribute offset = ts.addAttribute(OffsetAttribute.class);
            // 获取词元文本属性
            CharTermAttribute term = ts.addAttribute(CharTermAttribute.class);
            // 获取词元文本属性
            TypeAttribute type = ts.addAttribute(TypeAttribute.class);
            // 重置TokenStream（重置StringReader）
            ts.reset();
            // 迭代获取分词结果
            while (ts.incrementToken()) {
                Word word = new Word();
                word.setStartOffset(offset.startOffset());
                word.setEndOffset(offset.endOffset());
                word.setTerm(term.toString());
                word.setType(type.type());
                wordList.add(word);
            }
            // 关闭TokenStream（关闭StringReader）
            ts.end(); // Perform end-of-stream operations, e.g. set the final offset.

        } catch (IOException e) {
            log.error("IO error", e);
        } finally {
            // 释放TokenStream的所有资源
            if (ts != null) {
                try {
                    ts.close();
                } catch (IOException e) {
                    log.error("IO close error", e);
                }
            }
        }
        return wordList;
    }

    public class Word {
        // 获取词元位置属性
        private int startOffset;
        private int endOffset;
        // 获取词元文本属性
        private String term;
        // 获取词元文本属性
        private String type;

        public int getStartOffset() {
            return startOffset;
        }

        public void setStartOffset(int startOffset) {
            this.startOffset = startOffset;
        }

        public int getEndOffset() {
            return endOffset;
        }

        public void setEndOffset(int endOffset) {
            this.endOffset = endOffset;
        }

        public String getTerm() {
            return term;
        }

        public void setTerm(String term) {
            this.term = term;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

}

package com.demo.uitls;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池工厂类
 */
public enum ThreadPoolExecutorFactory {

    INSTANCE;
    /**
     * CPU工作线程数，即核数
     */
    private final int CPU_COUNT = Runtime.getRuntime().availableProcessors();

    /**
     * 线程池核心线程数
     */
    private int CORE_POOL_SIZE = CPU_COUNT * 5;

    private ExecutorService executorService = Executors.newFixedThreadPool(CORE_POOL_SIZE);

    public ExecutorService getThreadPool() {
        return executorService;
    }

}
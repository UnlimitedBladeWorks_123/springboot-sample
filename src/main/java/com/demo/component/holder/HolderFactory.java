package com.demo.component.holder;

import com.demo.uitls.MNSClientHelper;
import com.demo.uitls.OSSClientHelper;
import com.demo.uitls.SendCloudHelper;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author DongXL
 * @Create 2018-03-21 14:11
 */

@Configuration
public class HolderFactory {

    @Bean
    @ConfigurationProperties(prefix = "demo.mns")
    public MNSClientHelper getMNSClientHelper() {
        return new MNSClientHelper();
    }

    @Bean
    @ConfigurationProperties(prefix = "demo.sendcloud")
    public SendCloudHelper getSendCloudHelper() {
        return new SendCloudHelper();
    }

    @Bean
    @ConfigurationProperties(prefix = "demo.oss")
    public OSSClientHelper getOSSClientHelper() {
        return new OSSClientHelper();
    }


}

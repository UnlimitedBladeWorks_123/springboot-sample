package com.demo.component.interceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Author DongXL
 * @Create 2018-03-30 17:56
 */
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {
    @Bean
    public HandlerInterceptor getMyInterceptor() {
        return new MyInterceptor();
    }

    @Bean
    public HandlerInterceptor getLogInterceptor() {
        return new LogInterceptor();
    }

    //增加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getMyInterceptor())    //指定拦截器类
                .addPathPatterns("/**");        //指定该类拦截的url
        registry.addInterceptor(getLogInterceptor())    //指定拦截器类
                .addPathPatterns("/**");        //指定该类拦截的url
    }

}

package com.demo.component.mq;

import com.aliyun.mns.client.CloudQueue;
import com.demo.component.holder.HolderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

//@Component
public class MQSInitializing {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${demo.mns.queue.springbootDemoTest}")
    private String springbootDemoQueueName;
    @Resource
    private HolderFactory holderFactory;

    private static boolean loopFlag = true;

    /**
     * 初始化方法的注解方式  等同与init-method=init
     */
    @PostConstruct
    public void init() {
        CloudQueue springbootDemoQueue = holderFactory.getMNSClientHelper().getClient().getQueueRef(springbootDemoQueueName);
        Thread springbootDemoThread = new Thread(new SpringbootDemoQueueThread(springbootDemoQueue), "SpringbootDemoQueueThread");
        springbootDemoThread.start();
    }

    /**
     * 销毁方法的注解方式  等同于destory-method=destory
     */
    @PreDestroy
    public void destroy() {
        logger.info("销毁MQSInitializing,停止loop进程");
        loopFlag = false;
    }

    public static boolean isLoopFlag() {
        return loopFlag;
    }
}

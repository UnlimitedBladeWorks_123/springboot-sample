package com.demo.component.mq;

import com.alibaba.fastjson.JSON;
import com.aliyun.mns.client.CloudQueue;
import com.demo.service.DemoService;
import com.demo.uitls.SpringContextHolder;

/**
 * @Author DongXL
 * @Create 2018-03-20 18:05
 */
public class SpringbootDemoQueueThread extends BaseLoopQueueThread {

    @Override
    public boolean deal(String msgStr) {
        logger.info("SpringbootDemoQueueThread 接收到消息：{}", msgStr);
        DemoService demoService = SpringContextHolder.INSTANCE.getBean(DemoService.class);
        logger.info("SpringbootDemoQueueThread 调用service方法，返回数据：{}", JSON.toJSONString(demoService.getSpringbootDemoList()));
        return true;
    }

    public SpringbootDemoQueueThread(CloudQueue queue) {
        super(queue);
    }

}

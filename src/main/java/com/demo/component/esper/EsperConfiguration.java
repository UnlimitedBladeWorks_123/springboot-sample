package com.demo.component.esper;

import com.demo.esper.IEsperConfig;
import com.demo.model.common.DemoConstant;
import com.demo.model.dto.DemoEsperEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Component
public class EsperConfiguration {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IEsperConfig iEsperConfig;

    /**
     * 初始化esper隔离区配置
     */
    @PostConstruct
    public void initAllEPServiceIsolated() {
        try {
            iEsperConfig.initEPServiceIsolated(DemoConstant.DEMO_EVENT_TYPE_NAME, DemoConstant.RULE_GROUP_NAME, DemoEsperEvent.class);
        } catch (Exception e) {
            logger.error("esper规则初始化失败 failed, RULE_GROUP_NAME:{}", DemoConstant.RULE_GROUP_NAME);
            throw e;
        }

    }
}

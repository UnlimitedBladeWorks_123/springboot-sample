package com.demo.esper.event;

public abstract class AbstractEsperEvent {

    private Long eventId;

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }
}

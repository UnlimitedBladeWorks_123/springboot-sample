package com.demo.esper.statement;

import lombok.Data;

import java.io.Serializable;

@Data
public class EsperRuleConfig implements Serializable {

    private Long ruleId;
    private String ruleGroupName;
    private String statement;
    private String remark;
}

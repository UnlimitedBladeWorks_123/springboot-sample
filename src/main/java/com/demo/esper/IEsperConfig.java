package com.demo.esper;

import com.demo.esper.event.AbstractEsperEvent;
import com.espertech.esper.client.EPServiceProviderIsolated;

public interface IEsperConfig {

    /**
     * 初始化配置
     *
     * @param ruleGroupName
     * @param eventClass
     * @return
     */
    <T extends AbstractEsperEvent> EPServiceProviderIsolated initEPServiceIsolated(String eventTypeName, String ruleGroupName, Class<T> eventClass);

}

package com.demo.esper;

import com.demo.esper.event.AbstractEsperEvent;
import com.demo.esper.result.EsperResult;

public interface IEsper {

    /**
     * @param ruleGroupName
     * @param pojoEvent
     * @return 事件id
     */
    Long sendEvent(String eventTypeName, String ruleGroupName, AbstractEsperEvent pojoEvent);

    <T extends EsperResult> T getEsperResult(Long eventId, Class<T> resultClass);
}

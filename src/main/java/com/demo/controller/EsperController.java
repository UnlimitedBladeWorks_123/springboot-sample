package com.demo.controller;

import com.demo.esper.IEsper;
import com.demo.model.common.DemoConstant;
import com.demo.model.common.JsonResult;
import com.demo.model.dto.DemoEsperEvent;
import com.demo.model.dto.DemoEsperResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class EsperController {

    @Resource
    private IEsper iEsper;

    @GetMapping("/esper")
    public Object esper() {
        DemoEsperEvent event = new DemoEsperEvent();
        event.setAge(9);
        Long eventId = iEsper.sendEvent(DemoConstant.DEMO_EVENT_TYPE_NAME, DemoConstant.RULE_GROUP_NAME, event);
        return JsonResult.createSuccessResult(iEsper.getEsperResult(eventId, DemoEsperResult.class));
    }

}

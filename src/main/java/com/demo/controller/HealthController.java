package com.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author DongXL
 * @Create 2016-12-27 18:06
 * @ResponseBody
 */

@RestController
public class HealthController {

    @GetMapping(value = "chktomcat/DF34m716ce101A8cmD")
    public void chktomcat() {
        return;
    }

}

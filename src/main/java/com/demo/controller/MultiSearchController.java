package com.demo.controller;

import com.demo.model.common.JsonResult;
import com.demo.model.common.Pagination;
import com.demo.model.common.PaginationDTO;
import com.demo.model.dto.ProductInfoMoreDTO;
import com.demo.model.dto.SpringbootDemoMoreDTO;
import com.demo.service.DemoService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author DongXL
 * @Create 2017-12-15 18:30
 */
@RestController
public class MultiSearchController {
    @Resource
    private DemoService demoService;

    /**
     * 多表查询返回非entity对象, 查询条件必传，repository返回对象，{@link com.demo.repository.baoxian.SalesInsureProductRepository#getProductInfoMoreList(Integer)}
     *
     * @param type
     * @return
     */
    @GetMapping("/multiSearch1")
    public Object multiSearch1(Integer type) {
        List<ProductInfoMoreDTO> data = demoService.multiSearch1(type);
        return JsonResult.createSuccessResult(data);
    }

    /**
     * 多表查询返回非entity对象, 查询条件必传，repository返回Map，{@link com.demo.repository.baoxian.SalesInsureProductRepository#getProductInfoMoreListWithMap(Integer)} (Integer)}
     *
     * @param type
     * @return
     */
    @GetMapping("/multiSearch2")
    public Object multiSearch2(Integer type) {
        List<ProductInfoMoreDTO> data = demoService.multiSearch2(type);
        return JsonResult.createSuccessResult(data);
    }


    /**
     * 多表查询返回非entity对象, 查询条件非必传，repository返回对象，{@link com.demo.repository.baoxian.SalesInsureProductRepository#getProductInfoMoreListWithDynamicParams(Integer, Sort)} (Integer)}
     *
     * @param type
     * @return
     */
    @GetMapping("/multiSearchWithDynamicParams")
    public Object multiSearchWithDynamicParams(Integer type) {
        List<ProductInfoMoreDTO> data = demoService.multiSearchWithDynamicParams(type);
        return JsonResult.createSuccessResult(data);
    }


    /**
     * 分页 - 多表查询返回非entity对象, 查询条件非必传，repository返回对象，{@link com.demo.repository.baoxian.SalesInsureProductRepository#getProductInfoMoreListWithDynamicParams(Integer, Pageable)} (Integer)}
     *
     * @param pagination
     * @param type
     * @return
     */
    @GetMapping("/multiSearchWithDynamicParamsByPagination1")
    public Object multiSearchWithDynamicParamsByPagination1(Pagination pagination, Integer type) {
        PaginationDTO data = demoService.multiSearchWithDynamicParamsByPagination1(pagination, type);
        return JsonResult.createSuccessResult(data);
    }

    /**
     * 分页 - 多表查询返回非entity对象, 查询条件非必传，repository返回对象，{@link com.demo.repository.baoxian.SalesInsureProductRepository#getProductInfoMoreListWithDynamicParams(Integer, Pageable)} (Integer)}
     *
     * @param pagination
     * @param type
     * @return
     */
    @GetMapping("/multiSearchWithDynamicParamsByPagination2")
    public Object multiSearchWithDynamicParamsByPagination2(Pagination pagination, Integer type) {
        PaginationDTO<ProductInfoMoreDTO> data = demoService.multiSearchWithDynamicParamsByPagination2(pagination, type);
        return JsonResult.createSuccessResult(data);
    }


    @GetMapping("/multiDataSourceSearch")
    public Object multiDataSourceSearch() {
        List<SpringbootDemoMoreDTO> data = demoService.multiDataSourceSearch();
        return JsonResult.createSuccessResult(data);
    }

    @GetMapping("/shardingJdbcBaoxianSearch")
    public Object shardingJdbcBaoxianSearch(String orderNo) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("mainOrder", demoService.getInsurancePolicyOrderMainList(orderNo));
        retMap.put("demoList", demoService.getDemoByOrderNo(orderNo));
        return JsonResult.createSuccessResult(retMap);
    }

    @GetMapping("/shardingJdbcBaoxianTransaction")
    public Object shardingJdbcBaoxianTransaction(String orderNo) {
        demoService.saveMainOrder(orderNo);
        return JsonResult.createSuccessResult(null);
    }

}

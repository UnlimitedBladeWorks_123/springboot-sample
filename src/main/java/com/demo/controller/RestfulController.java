package com.demo.controller;

import com.demo.model.common.JsonResult;
import com.demo.model.dto.SpringbootDemoDTO;
import com.demo.service.DemoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author DongXL
 * @Create 2017-12-15 18:30
 */
@RestController
public class RestfulController {
    @Resource
    private DemoService demoService;

    /**
     * restful api - 新建对象
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/demo")
    public Object addSpringbootDemo(@RequestBody SpringbootDemoDTO dto) {
        SpringbootDemoDTO data = demoService.addSpringbootDemo(dto);
        return JsonResult.createSuccessResult(data);
    }

    /**
     * restful api - 删除对象
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/demo/{id}")
    public Object deleteSpringbootDemo(@PathVariable("id") Long id) {
        demoService.deleteSpringbootDemo(id);
        return JsonResult.createSuccessResult(null);
    }


    /**
     * restful api - 更新对象
     *
     * @param id
     * @param dto
     * @return
     */
    @PutMapping(value = "/demo/{id}")
    public Object updateSpringbootDemo(@PathVariable("id") Long id, @RequestBody SpringbootDemoDTO dto) {
        SpringbootDemoDTO data = demoService.updateSpringbootDemo(id, dto);
        return JsonResult.createSuccessResult(data);
    }

    /**
     * restful api - 获取对象
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/demo/{id}")
    public Object getSpringbootDemo(@PathVariable("id") Long id) {
        SpringbootDemoDTO data = demoService.getSpringbootDemo(id);
        return JsonResult.createSuccessResult(data);
    }

    /**
     * restful api - 获取对象列表
     *
     * @return
     */
    @GetMapping(value = "/demo")
    public Object getSpringbootDemoList() {
        List<SpringbootDemoDTO> data = demoService.getSpringbootDemoList();
        return JsonResult.createSuccessResult(data);
    }

}

package com.demo.controller;

import com.demo.esper.IEsperConfig;
import com.demo.model.common.DemoConstant;
import com.demo.model.common.JsonResult;
import com.demo.model.dto.DemoEsperEvent;
import com.espertech.esper.client.EPServiceProviderManager;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin")
@Slf4j
public class AdminController {

    @Resource
    private IEsperConfig iEsperConfig;

    @GetMapping("/refresh")
    public Object refresh() {
        EPServiceProviderManager.getProvider(DemoConstant.DEMO_EVENT_TYPE_NAME).destroy();
        try {
            iEsperConfig.initEPServiceIsolated(DemoConstant.DEMO_EVENT_TYPE_NAME, DemoConstant.RULE_GROUP_NAME, DemoEsperEvent.class);
        } catch (Exception e) {
            log.error("esper规则初始化失败 failed, RULE_GROUP_NAME:{}", DemoConstant.RULE_GROUP_NAME);
        }
        return JsonResult.createSuccessResult(null);
    }

}

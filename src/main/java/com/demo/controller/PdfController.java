package com.demo.controller;

import com.demo.uitls.SnowflakeUtil;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
public class PdfController {

    @RequestMapping(value = "pdfTest")
    public String pdfTest(Boolean downloadFlag, String output, Model model, HttpServletResponse response) {

        //下载文件名
        if (Boolean.TRUE.equals(downloadFlag)) {
            String fileName = String.format("%s.%s", SnowflakeUtil.INSTANCE.nextId(), output);
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s", fileName));
            response.setContentType("application/octet-stream");
        }
        model.addAttribute("money", 1122.123);
        if ("pdf".equals(output)) {
            //return pdf view (beanName)
            return "demoPdfView";
        } else {
            return "DemoPdf";
        }

    }
}

@Component("demoPdfView")
class DemoPdfView extends AbstractPdfView {

    @Override
    public String fetchViewName() {
        return "DemoPdf";
    }
}

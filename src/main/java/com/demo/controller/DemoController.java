package com.demo.controller;

import com.demo.model.common.JsonResult;
import com.demo.model.dto.PlanbookInsuranceCompanyDTO;
import com.demo.service.DemoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author DongXL
 * @Create 2017-12-15 18:30
 */
@RestController
public class DemoController {
    @Resource
    private DemoService demoService;

    /**
     * 熔断
     *
     * @param id
     * @return
     */
    @GetMapping("/circuitBreaker")
    public Object circuitBreaker(Long id) {
        PlanbookInsuranceCompanyDTO data = demoService.getPlanbookInsuranceCompany(id);
        return JsonResult.createSuccessResult(data);
    }

    /**
     * @apiVersion 1.0.0
     * @api {POST} /apiDocsSamples apiDoc示例接口
     * @apiGroup DemoController
     * @apiName apiDocsSamples
     * @apiParam (请求参数) {Object} samples 参数a
     * @apiParam (请求参数) {String} samples.m='222' 参数a.m
     * @apiParamExample 请求示例
     * {"samples":{"m"：1}}
     * @apiSuccess (响应参数)  {String} code 错误码
     * @apiSuccess (响应参数) {String} msg 错误信息
     * @apiSuccessExample 响应示例
     * {"code":200. "msg":"3232323"}
     * @apiSampleRequest 1
     */
    @PostMapping("/apiDocsSamples")
    public Object apiDocsSamples() {
        return JsonResult.createSuccessResult("");
    }

}

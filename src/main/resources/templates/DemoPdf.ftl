<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
        body, h1, h2, h3, h4, h5, h6, hr, p, blockquote, dl, dt, dd, ul, ol, li, pre, form, fieldset, legend, button, input, textarea, th, td {
            margin: 0;
            padding: 0;
        }

        body, button, input, select, textarea {
            color: #666;
        }

        body {
            background: #f0f0f0;
        }

        h1, h2, h3, h4, h5, h6 {
            font-size: 100%;
        }

        address, cite, dfn, em, var {
            font-style: normal;
        }

        small {
            font-size: 12px;
        }

        ul, ol {
            list-style: none;
        }

        a {
            text-decoration: none;
            color: #656565;
        }

        a:hover {
            text-decoration: underline;
            color: #D01621;
        }

        sup {
            vertical-align: text-top;
        }

        sub {
            vertical-align: text-bottom;
        }

        fieldset, img {
            border: 0;
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .clear {
            clear: both;
        }

        #main {
            margin: auto;
            padding: 10px;
        }

        #top {
            margin: auto;
            padding: 20px 10px 10px;
        }

        #top .item {
            margin: auto;
            padding: 10px 50px;
        }

        #middle {
            margin: auto;
            padding: 10px;
        }

        #middle .item {
            margin: auto;
            padding: 10px 25px 5px;
        }

        #middle .insure_item {
            margin: auto;
            padding: 8px 25px 5px;
        }

        #middle .insure_item_0 {
            width: 40%;
            float: left;
        }

        #middle .insure_item_1 {
            width: 50%;
            float: right;
        }

        #middle .insure_item_00 {
            float: left;
        }

        #middle .insure_item_01 {
            float: right;
        }

        .summary {
            color: #337ab7;
            text-align: right;
            padding-right: 50px;
        }

    </style>
</head>
<body style="font-size:12.0pt; font-family: 宋体, 华文细黑, MS Mincho,Times New Roman, serif;">

<div id="main">
    <div id="middle">
        <label>保费计算：${money}</label>
    </div>
</div>

</body>
</html>